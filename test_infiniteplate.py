
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata

from fun_grav import poly, gz_poly3d


"""
It calculates the gravitacional anomaly produced for a
parallelepiped to test the functions poly and gz_poly
"""

"""
x = np.linspace(1, 100, 20)
y = np.linspace(1, 100, 20)
X, Y = np.meshgrid(x, y)
Z = np.zeros(X.shape)
"""
X, Y, Z = (np.array([[0]]), np.array([[0]]), np.array([[100]]))

xpoly = [50]
ypoly = [0]
h = np.array([-100])
z = np.array([0])

xface, yface, zface = poly(xpoly, ypoly, h, z, delta=100000000)

fig = plt.figure()
ax1 = fig.add_subplot(111, projection='3d')
print(xface, yface, zface)
# calculate gravitational anomaly for the bodies
rho = 2670
gz = 0
# cicle over the bodies
for i, xf in enumerate(xface):
    gz += gz_poly3d(X, Y, Z, rho,
                    xface[i], yface[i], zface[i])
gz_mgal = gz*10**5

maxgzpoly = gz_mgal
infplate = 2*np.pi*rho*6.67408*10**-11*10**5*(z-h)
deltagz = maxgzpoly - infplate
print(f"Delta Gz between the parallelepiped and a infinite playe: {deltagz}")
