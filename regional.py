
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import cartopy.io.img_tiles as cimgt

import cartopy.crs as ccrs


def plot_regional3D(X, Y, Z, fignum=1):
    f3d = plt.figure(fignum)
    ax = f3d.add_subplot(111, projection='3d')

    # plot the original points. We use zip to get 1D lists of x, y and z
    # coordinates.
    ax.plot_surface(X, Y, Z, cmap='plasma')
   
    plt.tight_layout()

    return f3d


def plot_regional2D(X, Y, Z, fignum=1):
    f2d = plt.figure(fignum)
    ax = plt.axes(projection=ccrs.UTM(19, southern_hemisphere=True))
     # mask nan values
    Z_mapa = np.ma.masked_array(Z, mask=np.isnan(Z))

    ax.set_extent((X.min()-2000, X.max()+2000,
                   Y.min()-1000, Y.max()+1000),
                   crs=ccrs.UTM(zone=19, southern_hemisphere=True))
    # plot grid
    cmap = plt.cm.coolwarm

    # Create a Stamen Terrain instance.
    stamen_terrain = cimgt.StamenTerrain()
    # Add the Stamen data at zoom level 8.
    ax.add_image(stamen_terrain, 11)

    # colorbar scale
    im = ax.contourf(X, Y, Z_mapa, cmap=cmap, vmin=Z_mapa.min(),
                     vmax=Z_mapa.max(), alpha=0.5, zorder=1)

    cbax = plt.axes(projection=ccrs.UTM(19, southern_hemisphere=True))
    cbar = f2d.colorbar(im)
    latmin = Y.min() - 2000
    latmax = Y.max() + 2000
    lonmin = X.min() - 1000
    lonmax = X.max() + 1000
    paralelos = np.arange(int(latmin), int(latmax), 2000)
    meridianos = np.arange(int(lonmin), int(lonmax), 2000)
    ax.set_xticks(meridianos)
    ax.set_yticks(paralelos)
    cbar.set_label('regional [mGal]')

    plt.grid()
   
    return f2d


def remove_regional(x, y, z, plot=False, fignums=1):
    # creates a plane
    v1 = [324349.0, 6253807.0, 86.62]
    v2 = [322515.0, 6254448.0, 88.33]
    v3 = [221403.0, 6252062.0, 95.05]

    cp = np.cross(v1, v2)
    d = np.dot(cp, v3)
    a, b, c = cp
    z_regional = (a*x + b*y - d)/c

    # optional plot the regional plane
    if plot:
        regional3d = plot_regional3D(x, y, z_regional, fignum=fignums)
        regional2d = plot_regional2D(x, y, z_regional, fignum=fignums+1)

    new_gz = z -  z_regional

    return new_gz


