
import re
import pdb

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

def recta(x, m, c):
    """
    Auxiliar and private function, returns a straight line
    y = m*x + c
    """
    return x*m+c


def load_profile(fname):
    """
    Load the data in the file "fname"

    RETURNS
    xyz:     x, y, z coordinates of the profile (longitude and latitude)
    d   :   Acumulated distance between points in the profile
    gz   :   Gravitacional anomaly
    """
    # load data
    data = np.loadtxt(fname, skiprows=4).T
    gz = data[4]
    
    # perfil representatitvo
    popt, pcov = curve_fit(recta, data[0], data[1])
    xyz = (data[0], recta(data[0], popt[0], popt[1]), data[3])

    # calculates the distance between points
    d = [np.sqrt(((xyz[0][x]-xyz[0][x+1])**2)+((xyz[1][x]-xyz[1][x+1])**2))
         for x in range(len(xyz[1])-1)]
    d = np.hstack(([0], d))

    # calculate the acumulated distnace between points
    for di in range(len(d)-1):
        d[di+1] = d[di] + d[di+1]

    return data, xyz, d, gz


def load_grid(filename):
    """
    Load a grid data from oasis XYZ with lines comments
    """
    with open(filename,'r') as f:
        data_x = []
        data_y = []
        data_z = []
        data_gz = []
        xlines = []
        ylines = []
        zlines = []
        gzlines = []
        line_number = 2
        for row in f:
            if row == f'Line  {line_number}\n':
                data_x.append(np.array(xlines))
                data_y.append(np.array(ylines))
                data_z.append(np.array(zlines))
                data_gz.append(np.array(gzlines)) 
                xlines = []
                ylines = []
                zlines = []
                gzlines = []
                line_number += 1
                continue
            matchobj = re.search('\s{1}(\d{6}\.\d{2})\s{1}(\d{7}\.\d{2})\s*(\d{3}\.\d{2})\s*(-?\d{1,2}\.\d{2}|\*{1})', row)
            x = float(matchobj[1])
            y = float(matchobj[2])
            z = float(matchobj[3])
            if matchobj[4] == '*':
                gz = np.nan
            else:
                gz = float(matchobj[4])
            xlines.append(x)
            ylines.append(y)
            zlines.append(z)
            gzlines.append(gz)
    f.close()
    # save the last cycle
    data_x.append(np.array(xlines))
    data_y.append(np.array(ylines))
    data_z.append(np.array(zlines))
    data_gz.append(np.array(gzlines))
    # final arrays
    data_x = np.array(data_x)
    data_y = np.array(data_y)
    data_z = np.array(data_z)
    data_gz = np.array(data_gz)

    return data_x, data_y, data_z, data_gz
