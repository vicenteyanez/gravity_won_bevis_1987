
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata

from fun_grav import poly, gz_poly3d


"""
Test that calculates the gravitacional anomaly produced for a
parallelepiped.
Test functions poly and gz_poly
"""


x = np.linspace(1, 100, 20)
y = np.linspace(1, 100, 20)
X, Y = np.meshgrid(x, y)
Z = np.zeros(X.shape)

xpoly = [50]
ypoly = [50]
h = np.array([100])
z = np.array([1])

xface, yface, zface = poly(xpoly, ypoly, z, h, delta=10)

# calculate gravitational anomaly for the bodies
rho = 2670
gz = 0
# cicle over the bodies
for i, xf in enumerate(xface):
    gz += gz_poly3d(X, Y, Z, rho,
                    xface[i], yface[i], zface[i])
gz_mgal = gz*10**5

# gravity plot
fig = plt.figure()
ax1 = fig.add_subplot(111, projection='3d')
ax1.plot_surface(X, Y, gz_mgal, cmap='plasma')
plt.show()

