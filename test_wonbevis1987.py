
import json
import os

import matplotlib.pyplot as plt
import numpy as np

from fun_grav import gz_poly
from load_data import load_profile


def main():
    """
    Test code for my implementation of Won and Bevis (1987) gravitational
    anomaly solution due to an n-sided polygon in a two dimensional space

    IMPORTANT
    The last vertice in the modelfile.json should be equal to the first vectice
    """
    
    with open('test.json') as f:
        mydata = json.load(f)
    d = np.arange(0, 300, 10)
    zs = np.zeros(30)
        # define plot objects
    f1, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=(15, 8))

    # loop over the bodies
    gzt = 0  # variable to save the gravity
    for b in mydata["List of rectangular bodies"]:
        gzt += gz_poly(d, zs, mydata[b]["xvertice"],
                       mydata[b]["zvertice"],
                       mydata[b]["density"])  # density in kg/m^3
        # anomaly a body with density 2.67 g/cc
        gzt -= gz_poly(d, zs, mydata[b]["xvertice"],
                       mydata[b]["zvertice"], 2670)  # density in kg/m^3
        # plot model polygons
        ax1.plot(mydata[b]["xvertice"], mydata[b]["zvertice"], 'o-', label=b)

    ax1.plot(d, zs, 'o', label='observation points')
    ax1.set_ylabel('altitude(m)')
    ax1.set_xlabel('m')
    ax1.set_title('Model')

    # Plot to compare the data and the predicted anomaly
    ax2.axhline(0, color='#9D9D9D', linestyle='--', linewidth=0.5)

    # figure 2
    ax2.plot(d, gzt, 'ro-', label='Model')  # grav anomaly predicted
    ax2.set_ylabel('mGal')
    ax2.set_title('Gravitational Anomaly')

    # add legend and plot
    ax1.legend()
    ax2.legend()
    plt.show()


main()
