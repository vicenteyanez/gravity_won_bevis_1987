
import math
import pdb

import numpy as np


def gz_poly(xs, zs, xv, zv, den):
    """
    # ###############################################################
    Documentacion original de Won and Bevis 1987
    # ###############################################################
    subrutine gz_poly(xs, zs, xv, zv, den)

    Computes the vertical component of gravitational acceleration fue 
    to a polygon in a two-dimensional space (X, Z), or equivalently,
    fue to an infinitely-long polygonal cylinder strinkin in the 
    y-direction in a three-dimensional space (X, Y, Z)

    ARGUMENTS
        xs, zs  Vectors containing the station coordinates (first 
                station is at [xs[0], zs[0], etc])
        xv, zv  Vectors containing the coordinates of the polygon
                vertices in clockwise sequence
        den     the polygon density (kg/m^3) 
    RETURNS
        grav_z  vector containing z-component of gravitational
                acceleration due to the polygon at each of the
                stations (mGal)

    SIGN CONVENTION
    The z-axis is positive downwards, the x-axis is positive to the
    right.
    """


    # variables
    G = 6.67408*10**-11  # m^3/(kg*s^2)
    twogro = 2*G*den
    nvert = len(xv)

    # variable to save the data
    gz = []

    # loop in the stations coordinates
    for stationx, stationz in zip(xs, zs):
        gz_i = 0  # initial value of gz_i

        # loop in the polygon vertices
        for j in range(nvert-1):
            # vertice 1 and vertice 2
            xvert1 = xv[j] - stationx  # put i-station on (0, 0)
            zvert1 = zv[j] - stationz
            xvert2 = xv[j+1] - stationx
            zvert2 = zv[j+1] - stationz

            r2 = np.sqrt(xvert2**2+zvert2**2)
            r1 = np.sqrt(xvert1**2+zvert1**2)

            # theta values
            theta1 = np.arctan2(zvert1, xvert1)
            theta2 = np.arctan2(zvert2, xvert2)

            # Conditional for the three cases in Won and Bevis 1987
            # case 1
            # conditional for theta values if z1 different sign than z2
            if (zvert1 < 0 and zvert2 > 0) or (zvert1 > 0 and zvert2 < 0):
                if xvert1*zvert2 < xvert2*zvert1 and zvert2 >= 0:
                    theta1 = theta1 + 2*np.pi
                elif xvert1*zvert2 > xvert2*zvert1 and zvert1 >= 0:
                    theta2 = theta2 + 2*np.pi
                elif xvert1*zvert2 == xvert2*zvert1:
                    continue
            # case 2
            if (xvert1 == 0 and zvert1 == 0) or (xvert2 == 0 and zvert2 == 0):
                continue
            # case 3
            if xvert1 == xvert2:
                gz_i += xvert1*np.log(r2/r1)
                continue

            # if all the cases have passed
            A = (xvert2-xvert1)*(xvert1*zvert2-xvert2*zvert1)/(
                ((xvert2-xvert1)**2)+(zvert2-zvert1)**2)
            B = (zvert2 - zvert1)/(xvert2-xvert1)
            gz_i += A*(theta1 - theta2 + B*np.log(r2/r1))

        gz.append(gz_i)
    # multiply element-wise the values of gz with 2*G*rho
    gz = np.array(gz)*twogro*10**5  # 10^5 to transform it to mgal

    return gz


def gz_poly3d(x0, y0, z0, rho, xp, yp, zp):
    """
    Calculates the anomaly for one parallelepiped over a
    grid with (x0, y0) points.
    x0  (numpy.array 2D) : x coordinates of the observation points
    y0  (numpy.array 2D) : y coordinates of the observation points
    rho (float)          : parallelepiped of constant density
    xp   (numpy.array)   : polygon x coordinates of lenght 2
    yp   (numpy.array)   : polygon y coordinates of lenght 2
    zp   (numpy.array)   : polygon z coordinates of lenght 2

    Output
    gz_total (numpy.array 2D)  : gravity values for each one of the
                                 grid points
    """
    G = 6.67408*10**-11  # m^3/(kg*s^2)

    gz_total = []
    # cicle over the observation points
    for n, x_row in enumerate(x0):
        # body coordinates for the n-row
        # of observation points
        x = np.array([x-xp for x in x0[n]]).T
        y = np.array([y-yp for y in y0[n]]).T
        z = np.array([z-zp for z in z0[n]]).T

        # calculate gravity
        gz=0
        for i in [0, 1]:
            for j in [0, 1]:
                for k in [0, 1]:
                    mu = ((-1)**(i+1))*((-1)**(j+1))*((-1)**(k+1))
                    R = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
                    arg1 = np.arctan2(x[i]*y[j], z[k]*R)
                    for n, arg1_i in enumerate(arg1):
                        if arg1[n] < 0:
                            arg1[n] += 2*np.pi
                    gz += mu*(z[k]*arg1 - x[i]*np.log(R+y[j]) -
                             y[j]*np.log(R+x[i])) 
        gz = G*rho*gz
        gz_total.append(gz)
    gz_total = np.array(gz_total)
    
    return gz_total


def bott_inversion2d(g_obs, rho, x, z,  n_iter=6):
    """
    The docs
    g_obs     :  numpy.array. Values of measured gravity
    rho       :  float. Body density
    x, z      :  numpy.array. Data coordinates  
    n_iter    :  int. Optional
    """
    G = 6.67408*10**-11  # m^3/(kg*s^2)    
    hi = g_obs*10**-5/(2*np.pi*rho*G)  # 10^-5 transform it to mgal

    final_results = []
    for i in range(n_iter):
        # bodies with height hi
        bodies = rectangles(x, z, hi)

        # theoretical gravity
        g_teo_i = 0
        for body in bodies:
            g_teo_i += gz_poly(x, z, body[0], body[1], rho)

        # new height and polygons
        delta_hi =(g_obs-g_teo_i)*10**-5/(2*np.pi*rho*G)
        hi += delta_hi

        # save final iteration
        if i == n_iter-1:
            bodies = rectangles(x, z, hi)
            g_final = 0
            for body in bodies:
                g_final += gz_poly(x, z, body[0], body[1], rho)
            final_results.append([g_final, hi, bodies])
 
    return final_results[0][0], final_results[0][1], final_results[0][2]


def bott_inversion3d(g_obs, rho, x, y, z, n_iter=6, delta=10):
    """
    The docs
    g_obs      :  numpy.array. Values of measured gravity [m/s2]
    rho        :  float. Body density
    x, y, z    :  2D numpy.array. Data coordinates of the grid  
    n_iter     :  int. Optional
    delta      :  size of the polygon in the x-y plane
    """
    G = 6.67408*10**-11  # m^3/(kg*s^2)    
    hi = g_obs/(2*np.pi*rho*G) + z
    x_ravel = np.ravel(x)
    y_ravel = np.ravel(y)

    # cycle over the iteration number
    for i in range(n_iter):
        z_ravel = np.ravel(z)
        hi_ravel = np.ravel(hi)
        xp, yp, zp = poly(x_ravel, y_ravel, z_ravel, hi_ravel,
                          delta=delta)
 
        # theoretical gravity
        g_teo_i = 0
        # cycle over all the parallelepiped
        for j, xpi in enumerate(xp):
            # progress info
            if ((j+1)%100) == 0.:
                print(f'Body {j+1} of {len(xp)}')
             
            # skip the points with nan values
            if np.isnan(zp[j][0]) or np.isnan(zp[j][1]):
                continue
            
            g_teo_i += gz_poly3d(x, y, z, rho,
                                 xp[j], yp[j], zp[j])
                
        # new height and polygons
        delta_g = g_obs-g_teo_i
        delta_hi = delta_g/(2*np.pi*rho*G)
        # mask nan values to obtain stats
        hi_m = np.ma.masked_array(hi, mask=np.isnan(hi))
        delta_g_m = np.ma.masked_array(delta_g,
                                       mask=np.isnan(delta_g))
        delta_hi_m = np.ma.masked_array(delta_hi,
                                        mask=np.isnan(delta_hi))
        print(f"Iteration number {i+1}")
        print(f"hi max: {hi_m.max()}")
        print(f"hi min: {hi_m.min()}")
        print(f"Delta g max: {delta_g_m.max()}")
        print(f"Delta height max: {delta_hi_m.max()}")
        print(f"Delta height min: {delta_hi_m.min()}")
        hi += delta_hi

        # check that hi < z
        for j, hi_j in enumerate(hi):
            for k, hi_k in enumerate(hi[i]):
                if hi[j][k] < z[j][k]:
                    hi[j][k] = z[j][k]

    # final parallelepiped
    xp, yp, zp = poly(x_ravel, y_ravel, z_ravel,  np.ravel(hi),
                      delta=delta)
    g_final = 0
    for j, xpi in enumerate(xp):
        # skip the points with nan values
        if np.isnan(zp[j][0]) or np.isnan(zp[j][1]):
            continue
        g_final += gz_poly3d(x, y, z, rho,
                              xp[j], yp[j], zp[j])
 
    return g_final, hi, (xp, yp, zp)


def rectangles(x, z, h, y=False):
    """
    Creates rectangles from a given
    grid
    Input
    x : 1d numpy array vector with the lenght
    z : 1d numpy array vector with the ceiling
    h : 1d numpy array vector with the height (floor)
    y : required for 3d, 1d numpy array vector with the width
    cellsize : required only for the 3d version
    """
    if y is False:
        bodies = []
        for i, xi in enumerate(x):
            # final iteration
            if i+1 == len(x):
                delta_x = (x[i] - x[i-1])/2
            else:
                delta_x = (x[i+1] - x[i])/2
            # first iteration
            if i == 0:
                delta_x_neg = -delta_x
            else:
                delta_x_neg = (x[i-1]-x[i])/2
            
            xvert = [x[i]+delta_x_neg, x[i]+delta_x,
                     x[i]+delta_x, x[i]+delta_x_neg,
                     x[i]+delta_x_neg] 
            zvert = [z[i], z[i],
                     z[i]-h[i], z[i]-h[i],
                     z[i]] 
            bodies.append([xvert, zvert])
        bodies = np.array(bodies)
    return bodies


def poly(x, y, z1, z2, delta=10):
    """
    It must be a regular grid
    x, y, z, h  : 1d array, parallelepiped coordinates
                  z1 < z2
    delta       : int or float. Indicates the x-y
                  sizes of the parallelepiped
    Returns
    xcoord, ycoord, zcoord : 2D array, 2 x n
                             n: lenght of x, y, z  
    """
    xcoord = [[x[i]-delta, x[i]+delta] for i, xi in enumerate(x)]
    ycoord = [[y[i]-delta, y[i]+delta] for i, yi in enumerate(y)]
    zcoord = [[z1[i], z2[i]] for i, zi in enumerate(z1)]
    bodies = np.array([xcoord, ycoord, zcoord])
    
    return xcoord, ycoord, zcoord


