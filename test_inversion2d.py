
import os

import numpy as np
import matplotlib.pyplot as plt

from fun_grav import bott_inversion2d
from load_data import load_profile
from explore_data import location_maps

# profile
fname = '{}/data/cuenca_santiago/perfil.XYZ'.format(os.path.dirname(os.path.abspath(__file__)))
raw_data, (xs, ys, zs), d, gz_data_original = load_profile(fname)
# plot locations maps
# f0 = location_maps(raw_data, (xs, ys, zs), d)
# remove regional anomaly
z_regional_model = (-2.55*10**-3)*d+92  # optional. From yañez et al 2015
z_regional = ((gz_data_original[-1]-gz_data_original[0])/(d[-1]-d[0]))*d + gz_data_original[0]  # regional 
gz_data = gz_data_original - z_regional
gzt, h, bodies = bott_inversion2d(gz_data, rho=-700, x=d, z=zs, n_iter=3)

# define plot objects
f1, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, figsize=(15, 8))

# plot 1: Regional and original data
ax1.set_title('Regional')
ax1.set_ylabel('mGal')
ax1.plot(d, z_regional, label='Regional')
ax1.plot(d, gz_data_original, label='Original Data')

# Plot to compare the data and the model predicted anomaly
ax2.axhline(0, color='#9D9D9D', linestyle='--', linewidth=0.5)
ax2.plot(d, gz_data, 'bo-', label='Data')  # gravitation anomaly daota
ax2.plot(d, gzt, 'ro-', label='Model')  # grav anomaly predicted
ax2.set_ylabel('mGal')
ax2.set_title('Gravitational Anomaly')
# Final body
ax3.set_ylabel('altitude(m)')
ax3.set_xlabel('m')
# ax3.invert_yaxis()
ax3.set_title('Model')
for body in bodies:
    ax3.plot(body[0], body[1], 'b-')


# add legend and plot
ax1.legend()
ax2.legend()
ax3.legend()

plt.show()
