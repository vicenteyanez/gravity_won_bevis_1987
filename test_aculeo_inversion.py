
import os
import pdb

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata

from load_data import load_grid
from explore_data import plot_surface, plot_basin
from regional import remove_regional, plot_regional2D, plot_regional3D
from fun_grav import poly, gz_poly3d, bott_inversion3d

# load data
localpath = os.path.dirname(os.path.realpath(__file__))
data_path = f'{localpath}/data/'
aculeo_file = f'{data_path}laguna_aculeo/BC_STRM1_notrend__mask_dummies_100m.XYZ'
stations = f'{data_path}laguna_aculeo/data_points.XYZ'
x, y, z, gz = load_grid(aculeo_file)

stat_x, stat_y = np.loadtxt(stations, unpack=True)

# optional, interpol in a thicker grid to test the function
lenght = 20
X = np.linspace(x.min(), x.max(), lenght)
Y = np.linspace(y.min(), y.max(), lenght)
X, Y = np.meshgrid(X, Y)
Z = griddata((np.ravel(x), np.ravel(y)), np.ravel(z), (X, Y), method='linear')
GZ = griddata((np.ravel(x), np.ravel(y)), np.ravel(gz), (X, Y), method='linear')

fig_anomaly = plot_surface(x, y, gz, fignum=1)
plt.plot(stat_x, stat_y, 'ko')

X, Y, Z, GZ = (x, y, z, gz)  # comment it to use the thicker grid
delta=(X[0][1]-X[0][0])/2
GZ = GZ*10**-5

# invert
print("starting inversion")
rho = -700
gz_inv, h_inv, b_inv = bott_inversion3d(GZ, rho, X, Y, Z,
                                        delta=delta, n_iter=5)

residual = (GZ-gz_inv)*10**5

# mask results
h_inv = np.ma.masked_array(h_inv, mask=np.isnan(h_inv))
residual = np.ma.masked_array(residual, mask=np.isnan(residual))
gz = np.ma.masked_array(gz, mask=np.isnan(gz))

# stats info
print(f"Max and min height: {h_inv.max()}, {h_inv.min()}")
print(f"Max and min residual {residual.max()}, {residual.min()}")

# inversion plot
fig_inv = plot_basin(X, Y, h_inv-Z, fignum=2)
plt.plot(stat_x, stat_y, 'ko')
fig_inv = plot_surface(X, Y, residual, fignum=3)
plt.plot(stat_x, stat_y, 'ko')
# regional fail :(
gz_regional = remove_regional(x, y, gz, plot=True, fignums=4)

plt.show()
# save data results
data2save = np.array([X.ravel, Y.ravel, h_inv.ravel, residual.ravel]).T
np.savetxt('results.txt', data2save)
