# Python Inversion Gravity Tools

Python implementation of the 2D gravity solution of a polygon (Won and Bevis, 1987), the gravity of one parallelepiped (Plouff, 1976) and the inversion algorithm of Bott (1960)

Won, I. J., & Bevis, M. (1987). Computing the gravitational and magnetic anomalies due to a polygon:Algorithm and Dortran subroutines. Geophysics, 52(2), 232–238. https://doi.org/10.1190/1.1442298


# Won & Bevis (1987)  Perfil
## Input

You need two extra files to be able to run the code. 

1. perfil.XYZ file: This is a text file. 
   * First 4 lines are for comments
   * Data start at the fifth line. Columns are separated by spaces.
   * x-coordinate(m) y-coordinate(m) n-station elevation(m) full-bouguer(mGal)

Note: Although the n-station column data is optional, the elevation in the 4th column and bouger in the 5th is mandatory.

2. modelfile.json: This is a json file with the bodies and their vertices. An example modelfile.json is given. 
   * Vertices are given clockwise.

## Example
* See test_santiago.py and test_inversion2d.py for more details (the gavity data is not included in the repository).
* Data plots and the cross section position. From top to bottom and left to right. a) Local map with the data of perfil.xyz (blue) and a straight line that represent in the best way the data (red). b) Topographic profile of the data in the perfil.xyz. c) Regional position. 

![Model Example Results 1](output/figure_1.png)


* From top to bottom: a) Regional gravity for the example. b) Gravitational anomaly in the data (blue) and the gravitational anomaly predicted by the model. c) Model polygons and topography. 

![Model Example Results 1](output/figure_2_v2.png)


* Same as the previous figure but using the Bott (1960) inversion algorithm.

![Model Example Results 1](output/test_inversion2d.png)


# Bott (1960) and Plouff (1976)

* For more details, see test_inversion3d.py and test_aculeo_inversion.py (the gavity data is not included in the repository).

## Input
* A four columns .XYZ file whit the columns: x-coordinate y-coordinate altitude gravitational anomaly. If you have problems loading your data, edit the load_grid() function in the load_data.py file.
* A 2 columns text file, with the meditions points coordinates.

## Example
* Aculeo lake inversion using Plouff (1976) and Bott (1960) algorithms.

![Model Example Results 1](output/inversion500contraste.png)
