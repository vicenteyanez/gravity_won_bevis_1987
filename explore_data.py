
import cartopy.crs as ccrs
from cartopy.io.img_tiles import StamenTerrain
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LightSource
import cartopy.io.img_tiles as cimgt


def location_maps(data, xyz, d):
    # #################################################################
    # Figures:
    # 01. Map with the profile from the data and the new
    # 02. Topography profile
    # 03. Regional Map with the position of the profile
    # #################################################################
    tiler = StamenTerrain()
    f0 = plt.subplots(figsize=(15, 15))
    grid = plt.GridSpec(2, 3, wspace=0.4, hspace=0.3)
    ax01 = plt.subplot(grid[0, :2],
                       projection=ccrs.UTM(zone=19, southern_hemisphere=True))
    ax01.plot(data[0], data[1], 'bo-', label='data and stations position')
    ax01.plot(xyz[0], xyz[1], 'ro-',
              label='my profile and stations projection')

    ax01_latmin_m = data[1].min() - 500
    ax01_latmax_m = data[1].max() + 500
    ax01_lonmin_m = data[0].min() - 500
    ax01_lonmax_m = data[0].max() + 500
    ax01.set_extent((ax01_lonmin_m, ax01_lonmax_m,
                     ax01_latmin_m, ax01_latmax_m),
                    crs=ccrs.UTM(zone=19, southern_hemisphere=True))

    plt.tick_params(bottom=True, top=True, left=True, right=True,
                    labelbottom=True, labeltop=False, labelleft=True,
                    labelright=False)

    ax01.xaxis.set_visible(True)
    ax01.yaxis.set_visible(True)
    ax01.grid(True)
    ax01.set_title('')
    ax01.set_xlabel('meters')
    ax01.set_ylabel('meters')

    ax02 = plt.subplot(grid[1, :2])
    ax02.plot(d, xyz[2], 'k-')
    ax02.plot(d, xyz[2], 'ko')
    ax02.set_xlabel('distance from the start of the profile (meters)')
    ax02.set_ylabel('altitude (meters)')

    ax02.set_title('Topography through the profile')

    ax03 = plt.subplot(grid[:, 2],
                       projection=ccrs.UTM(zone=19, southern_hemisphere=True))
    ax03_latmin_m = data[1].min() - 700000
    ax03_latmax_m = data[1].max() + 700000
    ax03_lonmin_m = data[0].min() - 300000
    ax03_lonmax_m = data[0].max() + 300000
    ax03.set_extent((ax03_lonmin_m, ax03_lonmax_m,
                     ax03_latmin_m, ax03_latmax_m),
                    crs=ccrs.UTM(zone=19, southern_hemisphere=True))
    ax03.plot(data[0][0], data[1][0], 'ko', label='Ubication')
    ax03.coastlines(resolution='10m')
    ax03.add_image(tiler, 6)

    plt.tick_params(bottom=True, top=True, left=True, right=True,
                    labelbottom=True, labeltop=False, labelleft=True,
                    labelright=False)
    ax03.xaxis.set_visible(True)
    ax03.yaxis.set_visible(True)
    ax03.grid(True)

    ax01.legend()
    ax02.legend()
    ax03.legend()
    return f0


def plot_surface(X, Y, Z, fignum=1):
    f2d = plt.figure(fignum)
    ax = plt.axes(projection=ccrs.UTM(19, southern_hemisphere=True))
     # mask nan values
    Z_mapa = np.ma.masked_array(Z, mask=np.isnan(Z))

    # plot grid
    cmap = plt.cm.coolwarm

    # Create a Stamen Terrain instance.
    stamen_terrain = cimgt.StamenTerrain()
    # Add the Stamen data at zoom level 8.
    ax.add_image(stamen_terrain, 11)

    latmin = Y.min() - 250
    latmax = Y.max() + 250
    lonmin = X.min() - 250
    lonmax = X.max() + 250
    
    paralelos = np.arange(int(latmin), int(latmax), 2000)
    meridianos = np.arange(int(lonmin), int(lonmax), 2000)
    ax.set_xticks(meridianos)
    ax.set_yticks(paralelos)

    ax.set_extent((lonmin, lonmax,
                   latmin, latmax),
                   crs=ccrs.UTM(zone=19, southern_hemisphere=True))
    # colorbar scale
    im = ax.pcolormesh(X, Y, Z_mapa, cmap=cmap, vmin=Z_mapa.min(),
                     vmax=Z_mapa.max(), alpha=1, zorder=1)

    cbax = plt.axes(projection=ccrs.UTM(19, southern_hemisphere=True))
    cbar = f2d.colorbar(im)
    cbar.set_label('[mGal]')
    plt.grid()
    return f2d


def plot_basin(X, Y, Z, fignum=1):   
    f2d = plt.figure(fignum)
    ax = plt.axes(projection=ccrs.UTM(19, southern_hemisphere=True))
     # mask nan values
    Z_mapa = np.ma.masked_array(Z, mask=np.isnan(Z))

    # Create a Stamen Terrain instance.
    stamen_terrain = cimgt.StamenTerrain()
    # Add the Stamen data at zoom level 8.
    ax.add_image(stamen_terrain, 11)

     # plot grid
    cmap = plt.cm.ocean

    latmin = Y.min() - 250
    latmax = Y.max() + 250
    lonmin = X.min() - 250
    lonmax = X.max() + 250
    paralelos = np.arange(int(latmin), int(latmax), 2000)
    meridianos = np.arange(int(lonmin), int(lonmax), 2000)
    ax.set_xticks(meridianos)
    ax.set_yticks(paralelos)

    ax.set_extent((lonmin, lonmax, latmin, latmax),
                   crs=ccrs.UTM(zone=19, southern_hemisphere=True))

    # colorbar scale
    im = ax.pcolormesh(X, Y, Z_mapa, cmap=cmap, vmin=Z_mapa.min(),
                     vmax=Z_mapa.max(), alpha=1, zorder=1)
    cbax = plt.axes(projection=ccrs.UTM(19, southern_hemisphere=True))
    cbar = f2d.colorbar(im)
    cbar.set_label('Sediment thickness [m]')
  
    plt.grid()
    return f2d
